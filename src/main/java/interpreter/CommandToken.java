package interpreter;

import interpreter.commands.InvalidCommandException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandToken {
    private String command;
    private String[] params;

    private static final Pattern COMMAND_PARAMS_PATTERN = Pattern.compile("([A-Za-z]+) (.+)");

    public CommandToken(String line) {
        Matcher matcher = COMMAND_PARAMS_PATTERN.matcher(line);

        if(matcher.matches()) {
            this.command = matcher.group(1);
            String params = matcher.group(2);
            params = deleteAllWhiteSpaces(params);

            this.params = params.split(",", -1);
        }
        else throw new InvalidCommandException(line, null);
    }

    public String getCommand() {
        return command;
    }

    public String[] getParams() {
        return params;
    }

    private String deleteAllWhiteSpaces(String str) {
        return str.replaceAll("\\s+", "");
    }

}
