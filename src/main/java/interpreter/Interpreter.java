package interpreter;

import interpreter.commands.Command;
import interpreter.commands.CommandFactory;
import interpreter.tokenizer.InvalidLineHandler;

import java.io.BufferedReader;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

public class Interpreter {

    private InvalidLineHandler invalidLineHandler;

    public void interpret(String line) {
        try {
            CommandToken commandToken = new CommandToken(line);
            Command cmd = CommandFactory.getCommand(commandToken);

            cmd.parse(commandToken.getParams());
        } catch(RuntimeException e) {
            if(invalidLineHandler != null) {
                invalidLineHandler.onInvalidLine(e);
            }
            else throw e;
        }
    }

    public void setCustomInvalidLineHandler(InvalidLineHandler invalidLineHandler) {
        this.invalidLineHandler = invalidLineHandler;
    }

    public void interpret(String[] lines) {
        for(String line : lines)
            interpret(line);
    }

    public void interpret(List<String> lines) {
        for(String line : lines)
            interpret(line);
    }

    public void interpret(InputStream streamOfLines) {
        Scanner scanner = new Scanner(streamOfLines);

        String line;
        while((line = scanner.nextLine()) != null) {
            interpret(line);
        }
    }

    public void interpret(BufferedReader streamOfLines) {
        Scanner scanner = new Scanner(streamOfLines);

        String line;
        while(scanner.hasNextLine() && (line = scanner.nextLine()) != null) {
            interpret(line);
        }
    }

}
