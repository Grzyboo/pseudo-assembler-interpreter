package interpreter;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        if(System.in.available() != 0) {
            interpretByType(3);
            return;
        }

        System.out.println("1. Line by line");
        System.out.println("2. File");
        System.out.print("Set interpret type: ");

        Scanner scanner = new Scanner(System.in);

        boolean invalidType = true;
        while(invalidType) {
            try {
                int type = scanner.nextInt();
                invalidType = false;
                interpretByType(type);
            } catch (InputMismatchException e) {
                System.out.print("Invalid type! ");
                scanner.next();
            }
        }
    }

    public static void interpretByType(int type) {
        switch(type) {
            case 1:
                try {
                    Interpreter interpreter = new Interpreter();
                    interpreter.interpret(System.in);
                } catch(NoSuchElementException e) {
                    System.out.println("Finishing job");
                }
                break;
            case 2:
                readFromFile();
                break;
            case 3:
                readFromStdInRedirectedFile();
                break;
            default:
                System.out.println("Error, closing program");
        }
    }

    public static void readFromFile() {
        Scanner scanner = new Scanner(System.in);

        boolean invalidFile = true;
        while(invalidFile) {
            System.out.print("File location: ");
            File file = new File(scanner.nextLine());
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
                deleteBOMCharacter(in);
                invalidFile = false;

                Interpreter interpreter = new Interpreter();
                interpreter.interpret(in);

            } catch (FileNotFoundException e) {
                System.out.println("Invalid file location!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void deleteBOMCharacter(BufferedReader in) throws IOException {
        in.mark(1);
        if (in.read() != 0xFEFF)
            in.reset();
    }

    private static void readFromStdInRedirectedFile() {
        Interpreter interpreter = new Interpreter();
        interpreter.setCustomInvalidLineHandler(e -> {
            System.out.println();
            System.out.println(e.getMessage());
        });
        Scanner scanner = new Scanner(System.in);

        if(scanner.hasNext()) {
            String line = scanner.nextLine().substring(3);  // trim BOM

            do {
                interpreter.interpret(line);
            } while (scanner.hasNext() && (line = scanner.nextLine()) != null);
        }
    }
}
