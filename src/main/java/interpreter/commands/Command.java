package interpreter.commands;

import interpreter.tokenizer.Tokenizer;

public abstract class Command {
    public abstract String name();
    protected abstract int paramsRequired();
    protected abstract void parseCommand(int param, Tokenizer tokenizer);

    public final void parse(String... params) {
        checkIfNumberOfParamsMatch(params);

        for(int i = 0; i < paramsRequired(); ++i) {
            Tokenizer tokenizer = new Tokenizer(params[i]);
            parseCommand(i, tokenizer);
            checkIfThereIsNoResidue(tokenizer);
        }
    }

    private void checkIfNumberOfParamsMatch(String[] params) {
        if(params.length != paramsRequired()) {
            throw new InvalidCommandParamsException("Invalid number of parameters. Expected " + paramsRequired() + ", got " + params.length);
        }
    }

    protected void checkIfThereIsNoResidue(Tokenizer tokenizer) {
        if(tokenizer.isAnythingLeftToParse()) {
            throw new InvalidCommandParamsException("Unexpected params " + tokenizer.getResidue());
        }
    }
}
