package interpreter.commands;

import interpreter.CommandToken;

import java.lang.reflect.Constructor;
import java.util.HashMap;


public class CommandFactory {

    private static HashMap<String, Class<?>> commandsMap = new HashMap<>();
    static {
        commandsMap.put("mov", Mov.class);
        commandsMap.put("push", Push.class);
        commandsMap.put("int", Int.class);
        commandsMap.put("xor", Xor.class);
    }

    private CommandFactory() {}

    public static Command getCommand(CommandToken commandToken) throws InvalidCommandException {
        try {
            return constructCommand(commandToken.getCommand());
        } catch (Exception e) {
            throw new InvalidCommandException(commandToken.getCommand(), e);
        }
    }

    private static Command constructCommand(String commandName) throws Exception {
        Class<?> clazz = commandsMap.get(commandName);
        Constructor<?> ctor = clazz.getConstructor();
        return (Command) ctor.newInstance();
    }
}
