package interpreter.commands;

import interpreter.simulation.SystemSim;
import interpreter.tokenizer.Token;
import interpreter.tokenizer.TokenValue;
import interpreter.tokenizer.Tokenizer;

class Int extends Command {

    public Int() { }

    @Override
    public String name() {
        return "int";
    }

    @Override
    public int paramsRequired() {
        return 1;
    }

    @Override
    public void parseCommand(int param, Tokenizer tokenizer) {
        TokenValue token = tokenizer.nextToken();

        if(token.getType() != Token.HEX_NUMBER)
            throw new InvalidCommandParamsException("Expected HEX value as an parameter for int");

        SystemSim.interrupt(Long.parseLong(token.getValue().replace("0x", ""), 16));
    }
}
