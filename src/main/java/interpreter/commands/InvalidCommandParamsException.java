package interpreter.commands;

public class InvalidCommandParamsException extends RuntimeException {
    public InvalidCommandParamsException(String message) {
        super(message);
    }

    public InvalidCommandParamsException(String message, Throwable cause) {
        super(message, cause);
    }
}
