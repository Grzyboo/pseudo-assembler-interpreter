package interpreter.commands;

import interpreter.simulation.SystemSim;
import interpreter.tokenizer.TokenInterpreter;
import interpreter.tokenizer.TokenValue;
import interpreter.tokenizer.Tokenizer;

class Mov extends Command {

    private long computedValue;

    public Mov() { }

    public String name() {
        return "mov";
    }

    public int paramsRequired() {
        return 2;
    }

    public void parseCommand(int param, Tokenizer tokenizer) {
        if(param == 0) {
            TokenInterpreter interpreter = new TokenInterpreter();
            computedValue = interpreter.compute(tokenizer.parseToTokenQueue());
        }
        else if(param == 1) {
            TokenValue tokenValue = tokenizer.nextToken();

            if(SystemSim.registry.nameBelongsToRegisters(tokenValue.getValue()))
                SystemSim.registry.setRegisterValue(tokenValue.getValue(), computedValue);
            else
                throw new InvalidCommandParamsException("Invalid param: " + tokenValue.getValue() + ", expected a register");
        }
    }
}
