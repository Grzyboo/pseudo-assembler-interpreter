package interpreter.commands;

import interpreter.simulation.SystemSim;
import interpreter.tokenizer.TokenInterpreter;
import interpreter.tokenizer.TokenValue;
import interpreter.tokenizer.Tokenizer;

import java.util.Queue;

class Push extends Command {

    public Push() { }

    @Override
    public String name() {
        return "push";
    }

    @Override
    public int paramsRequired() {
        return 1;
    }

    @Override
    public void parseCommand(int param, Tokenizer tokenizer) {
        TokenInterpreter tokenInterpreter = new TokenInterpreter();

        Queue<TokenValue> tokens = tokenizer.parseToTokenQueue();
        long value = tokenInterpreter.compute(tokens);

        SystemSim.push(value);
    }
}
