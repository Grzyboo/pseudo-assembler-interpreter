package interpreter.commands;

import interpreter.simulation.SystemSim;
import interpreter.tokenizer.TokenInterpreter;
import interpreter.tokenizer.Tokenizer;

public class Xor extends Command {

    private long firstValue;

    public Xor() { }

    @Override
    public String name() {
        return "xor";
    }

    @Override
    public int paramsRequired() {
        return 2;
    }

    @Override
    public void parseCommand(int param, Tokenizer tokenizer) {
        if(param == 0) {
            TokenInterpreter interpreter = new TokenInterpreter();
            firstValue = interpreter.compute(tokenizer.parseToTokenQueue());
        }
        else if(param == 1) {
            String outputRegister = tokenizer.nextToken().getValue();
            long secondValue = SystemSim.registry.getRegisterValue(outputRegister);
            long xor = firstValue ^ secondValue;
            SystemSim.registry.setRegisterValue(outputRegister, xor);
        }
    }
}
