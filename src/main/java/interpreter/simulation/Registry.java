package interpreter.simulation;

import java.util.HashMap;
import java.util.Map;

public class Registry {
    private Map<String, Long> registers = new HashMap<>();

    Registry() {
        registers.put("%eax", 0L);
        registers.put("%ebx", 0L);
        registers.put("%ecx", 0L);
        registers.put("%edx", 0L);
    }

    public void setRegisterValue(String registerName, long value) {
        if(nameBelongsToRegisters(registerName)) {
            registers.put(registerName, value);
        }
        else
            System.out.println("Invalid register: " + registerName);
    }

    public long getRegisterValue(String registerName) {
        Long value = registers.get(registerName);

        if(value == null) {
            System.out.println("Invalid register: " + registerName);
            return 0;
        }

        return value;
    }

    public boolean nameBelongsToRegisters(String name) {
        return registers.containsKey(name);
    }

    public void reset() {
        for(Map.Entry<String, Long> entry : registers.entrySet())
            entry.setValue(0L);
    }
}
