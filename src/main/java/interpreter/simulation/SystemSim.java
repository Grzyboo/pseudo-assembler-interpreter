package interpreter.simulation;

import java.util.Stack;

public class SystemSim {
    public static final Registry registry = new Registry();
    private static Stack<Long> stack = new Stack<>();

    private static long INT_DISPLAY = 0x80;

    private SystemSim() {}

    public static void push(long value) {
        stack.push(value);
    }

    public static long pop() {
        return stack.pop();
    }

    public static long peek() {
        return stack.peek();
    }

    public static void interrupt(long value) {
        if(value == INT_DISPLAY) {
            System.out.print(pop());
        }
    }

}
