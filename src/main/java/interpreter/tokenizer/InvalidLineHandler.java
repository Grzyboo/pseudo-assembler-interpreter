package interpreter.tokenizer;

/**
 * This interface is needed to set how the interpreter should behave on receiving an incorrect statement
 * You can use the parameter exception to e.g. print the message
 * If no exception is rethrown the interpreter will continue parsing other lines after handling the invalid line
 *
 * @param {exception} caught during interpreting the line.
 */
public interface InvalidLineHandler {
    void onInvalidLine(RuntimeException exception);
}
