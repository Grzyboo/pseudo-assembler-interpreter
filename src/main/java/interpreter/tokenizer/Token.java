package interpreter.tokenizer;

import java.util.regex.Pattern;

public enum Token {
    HEX_NUMBER("0x\\d+", true, false),
    NUMBER("\\d+", true, false),
    REGISTER("%e[a-d]x", true, false),
    ADD_OPERATOR("\\+", false, true),
    SUBTRACT_OPERATOR("-", false, true),
    MULTIPLY_OPERATOR("\\*", false, true),
    DIVIDE_OPERATOR("/", false, true),
    PARENTHESIS_LEFT("\\("),
    PARENTHESIS_RIGHT("\\)"),
    END_OF_COMMAND("");

    private String regex;
    private boolean numeric;
    private boolean operator;

    Token(String regex, boolean numeric, boolean operator) {
        this.regex = regex;
        this.numeric = numeric;
        this.operator = operator;
    }
    Token(String regex) {
        this.regex = regex;
        this.numeric = false;
        this.operator = false;
    }

    public String getRegex() {
        return regex;
    }

    public Pattern pattern() {
        return Pattern.compile(regex);
    }

    public Pattern extractionPattern() {
        return Pattern.compile("(" + regex + ")(.*)");
    }

    public boolean isNumeric() {
        return numeric;
    }

    public boolean isOperator() {
        return operator;
    }
}
