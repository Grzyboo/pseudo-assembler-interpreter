package interpreter.tokenizer;

import interpreter.commands.InvalidCommandParamsException;
import interpreter.simulation.SystemSim;

import java.util.ArrayDeque;
import java.util.EmptyStackException;
import java.util.Queue;
import java.util.Stack;

public class TokenInterpreter {

    /**
     *   Returns a queue of TokenValue that represent Reversed Polish Notation for given TokenValues
     *   The result queue does not contain parentheses
     */
    protected Queue<TokenValue> getRPN(Queue<TokenValue> tokens) {
        Queue<TokenValue> output = new ArrayDeque<>();
        Stack<TokenValue> stack = new Stack<>();

        TokenValue tokenValue;
        while((tokenValue = tokens.poll()) != null) {
            Token type = tokenValue.getType();

            if (type.isNumeric())
                output.add(tokenValue);
            else if (type.isOperator()) {
                if (type == Token.ADD_OPERATOR || type == Token.SUBTRACT_OPERATOR) {
                    while (stack.size() > 0 && (stack.peek().getType() == Token.MULTIPLY_OPERATOR || stack.peek().getType() == Token.DIVIDE_OPERATOR))
                        output.add(stack.pop());
                }
                stack.push(tokenValue);
            }
            else if (type == Token.PARENTHESIS_LEFT)
                stack.push(tokenValue);
            else if (type == Token.PARENTHESIS_RIGHT) {
                TokenValue operator = null;
                while (!stack.empty() && (operator = stack.pop()) != null && operator.getType() != Token.PARENTHESIS_LEFT)
                    output.add(operator);

                if(stack.empty() && operator != null && operator.getType() != Token.PARENTHESIS_LEFT)
                    throw new InvalidCommandParamsException("The parentheses are placed incorrectly. Too few 'LEFT' parentheses.");
            }
        }

        while(!stack.empty()) {
            TokenValue operator = stack.pop();
            if(operator.getType() == Token.PARENTHESIS_LEFT || operator.getType() == Token.PARENTHESIS_RIGHT)
                throw new InvalidCommandParamsException("The parentheses are placed incorrectly. Too few 'RIGHT' parentheses.");
            else
                output.add(operator);
        }

        return output;
    }

    public long compute(Queue<TokenValue> inputTokens) {
        try {
            Queue<TokenValue> tokens = getRPN(inputTokens);

            Stack<Long> stack = new Stack<>();

            TokenValue tokenValue;
            while ((tokenValue = tokens.poll()) != null) {
                Token type = tokenValue.getType();

                if (type.isNumeric())
                    stack.push(getNumericValue(tokenValue));
                else if (type.isOperator()) {
                    long val1 = stack.pop();
                    long val2 = stack.pop();
                    long result = operatorResult(type, val1, val2);
                    stack.push(result);
                }
            }

            if (stack.size() != 1)
                throw new IllegalArgumentException(); // It should never be called as long as RPN code is correct, however... just in case

            return stack.pop();
        }
        catch (EmptyStackException e) {
            throw new InvalidCommandParamsException("The given expression is invalid", e);
        }
    }

    private long operatorResult(Token token, long a, long b) {
        switch(token) {
            case ADD_OPERATOR:
                return b + a;
            case SUBTRACT_OPERATOR:
                return b - a;
            case MULTIPLY_OPERATOR:
                return b * a;
            case DIVIDE_OPERATOR:
                return b / a;
        }

        throw new IllegalArgumentException(); // It should never be called as long as RPN code is correct, however... just in case
    }

    private Long getNumericValue(TokenValue tokenValue) {
        switch (tokenValue.getType()) {
            case HEX_NUMBER:
                String val = tokenValue.getValue().replace("0x", "");
                return Long.parseLong(val, 16);
            case NUMBER:
                return Long.parseLong(tokenValue.getValue());
            case REGISTER:
                return SystemSim.registry.getRegisterValue(tokenValue.getValue());
        }

        return null;
    }
}
