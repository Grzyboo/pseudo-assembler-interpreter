package interpreter.tokenizer;

public class TokenValue {
    private Token type;
    private String value;

    public TokenValue(Token type, String value) {
        this.type = type;
        this.value = value;
    }

    public Token getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "(" + type + "," + value +")";
    }
}
