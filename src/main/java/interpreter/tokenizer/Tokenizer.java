package interpreter.tokenizer;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.regex.Matcher;

public class Tokenizer {
    private boolean finished;
    private String residue;

    public class FullyParsedException extends RuntimeException {}
    public class InvalidTokenException extends RuntimeException {
        private String residue;
        public InvalidTokenException(String residue) {
            super("Invalid token found at: " + residue);
            this.residue = residue;
        }

        public String getResidue() {
            return residue;
        }
    }

    public Tokenizer(String text) {
        this.residue = text;
    }

    public TokenValue nextToken() {
        if(finished)
            throw new FullyParsedException();

        if(residue.length() == 0) {
            finished = true;
            return new TokenValue(Token.END_OF_COMMAND, "");
        }

        for(Token token : Token.values()) {
            if(token == Token.END_OF_COMMAND) continue;

            Matcher matcher = token.extractionPattern().matcher(residue);

            if(matcher.matches()) {
                residue = matcher.group(2);
                return new TokenValue(token, matcher.group(1));
            }
        }

        throw new InvalidTokenException(residue);
    }

    public String getResidue() {
        return residue;
    }

    public boolean isAnythingLeftToParse() {
        return residue != null && residue.length() != 0;
    }

    public Queue<TokenValue> parseToTokenQueue() {
        Queue<TokenValue> tokens = new ArrayDeque<>();

        TokenValue token;
        while((token = nextToken()).getType() != Token.END_OF_COMMAND) {
            tokens.add(token);
        }

        tokens.add(new TokenValue(Token.END_OF_COMMAND, ""));
        return tokens;
    }
}
