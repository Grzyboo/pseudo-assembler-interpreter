package interpreter;

import interpreter.commands.Command;
import interpreter.commands.CommandFactory;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;

public class CommandParseTest {

    private CommandToken c;

    @Test
    public void parseLines_resultsInProperCommandObjects() {
        c = new CommandToken("mov (4+6)*2, %edx");
        assertEquals(c.getCommand(), "mov");
        assertEquals(c.getParams()[0], "(4+6)*2");
        assertEquals(c.getParams()[1], "%edx");

        c = new CommandToken("int 0x80");
        assertEquals(c.getCommand(), "int");
        assertEquals(c.getParams()[0], "0x80");

        c = new CommandToken("mov 4 - 1, %ecx");
        assertEquals(c.getCommand(), "mov");
        assertEquals(c.getParams()[0], "4-1");
        assertEquals(c.getParams()[1], "%ecx");

        c = new CommandToken("mov %ebx + 3 + %edx*2 + 5 - %ecx, %ecx");
        assertEquals(c.getCommand(), "mov");
        assertEquals(c.getParams()[0], "%ebx+3+%edx*2+5-%ecx");
        assertEquals(c.getParams()[1], "%ecx");

        c = new CommandToken("push %edx + %ecx + %ebx");
        assertEquals(c.getCommand(), "push");
        assertEquals(c.getParams()[0], "%edx+%ecx+%ebx");
    }

    @Test
    public void parseParameters_resultsInCorrectDecomposition() {
        Command push = CommandFactory.getCommand(new CommandToken("push 2+5*1"));
        Command mov = CommandFactory.getCommand(new CommandToken("mov %edx + 5, %edx"));
        Command interrupt = CommandFactory.getCommand(new CommandToken("int 0x80"));

        assertEquals(push.name(), "push");
        assertEquals(mov.name(), "mov");
        assertEquals(interrupt.name(), "int");
    }
}
