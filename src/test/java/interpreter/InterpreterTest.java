package interpreter;

import interpreter.commands.InvalidCommandParamsException;
import interpreter.simulation.SystemSim;
import interpreter.tokenizer.Tokenizer;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class InterpreterTest {

    @Test
    public void testCommandInt() {
        ByteArrayOutputStream systemOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(systemOut));
        SystemSim.push(10);
        SystemSim.push(5);
        SystemSim.push(-2);

        Interpreter i = new Interpreter();
        i.interpret("int 0x80");
        i.interpret("int 0x80");
        i.interpret("int 0x80");

        assertEquals("-2510", systemOut.toString());
    }

    @Test
    public void testCommandPush() {
        SystemSim.registry.setRegisterValue("%edx", 3L);
        SystemSim.registry.setRegisterValue("%eax", 1L);
        SystemSim.registry.setRegisterValue("%eax", 10L);

        Interpreter i = new Interpreter();
        i.interpret("push %edx");
        i.interpret("push %eax");

        assertEquals(10L, SystemSim.pop());
        assertEquals(3L, SystemSim.pop());
    }

    @Test
    public void testCommandXor() {
        SystemSim.registry.setRegisterValue("%eax", 123L);
        SystemSim.registry.setRegisterValue("%ebx", 42L);

        Interpreter i = new Interpreter();
        i.interpret("xor %ebx, %eax");
        i.interpret("xor 2 + 5 * 3, %ebx");

        assertEquals(SystemSim.registry.getRegisterValue("%eax"), (123L ^ 42L));
        assertEquals(SystemSim.registry.getRegisterValue("%ebx"), (42L ^ (2+5*3L)));
    }

    @Test
    public void testCommandMov() {
        SystemSim.registry.setRegisterValue("%ecx", 13L);

        Interpreter i = new Interpreter();
        i.interpret("mov (5*4)+5, %eax");
        i.interpret("mov %eax -26, %ebx");
        i.interpret("mov %ecx+1, %ecx");

        assertEquals(25L, SystemSim.registry.getRegisterValue("%eax"));
        assertEquals(-1L, SystemSim.registry.getRegisterValue("%ebx"));
        assertEquals(14L, SystemSim.registry.getRegisterValue("%ecx"));
    }

    @Test
    public void testInterpreter_givenInputProgram_resultsInCorrectComputation() {
        ByteArrayOutputStream systemOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(systemOut));
        SystemSim.registry.reset();



        Interpreter interpreter = new Interpreter();

        interpreter.interpret("mov (4+6)*2, %edx");
        assertEquals(20, SystemSim.registry.getRegisterValue("%edx"));

        interpreter.interpret("push %edx*2");
        assertEquals(40, SystemSim.peek());

        interpreter.interpret("int 0x80");
        assertEquals("40", systemOut.toString());

        interpreter.interpret("push %edx");
        assertEquals(SystemSim.registry.getRegisterValue("%edx"), SystemSim.peek());

        interpreter.interpret("push %ecx");
        assertEquals(SystemSim.registry.getRegisterValue("%ecx"), SystemSim.peek());

        interpreter.interpret("int 0x80");
        assertEquals("400", systemOut.toString());

        interpreter.interpret("int 0x80");
        assertEquals("40020", systemOut.toString());

        interpreter.interpret("mov 4 - 1, %ecx");
        assertEquals(3, SystemSim.registry.getRegisterValue("%ecx"));

        assertThrows(Exception.class, () -> interpreter.interpret("mov 4 - , %ecx"));

        interpreter.interpret("push %ecx");
        assertEquals(3, SystemSim.peek());

        interpreter.interpret("int 0x80");
        assertEquals("400203", systemOut.toString());

        interpreter.interpret("mov 24, %ecx");
        assertEquals(24, SystemSim.registry.getRegisterValue("%ecx"));

        interpreter.interpret("xor %ebx, %ebx");
        assertEquals(0, SystemSim.registry.getRegisterValue("%ebx"));

        interpreter.interpret("mov %ebx + 3 + %edx*2 + 5 - %ecx, %ecx");
        assertEquals(0+3+20*2+5-24, SystemSim.registry.getRegisterValue("%ecx"));

        interpreter.interpret("push %edx + %ecx + %ebx");
        assertEquals(20+24+0 /*         */, SystemSim.peek());

        interpreter.interpret("int 0x80");
        assertEquals("40020344", systemOut.toString());

        interpreter.interpret("push 215");
        assertEquals(215, SystemSim.peek());

        interpreter.interpret("int 0x80");
        assertEquals("40020344215", systemOut.toString());

        assertThrows(Tokenizer.InvalidTokenException.class, () -> interpreter.interpret("mov 215, addddd"));

        assertThrows(InvalidCommandParamsException.class, () -> interpreter.interpret("mov %ecx, 215"));
    }

    @Test
    public void testExampleFile() {
        File file = new File("example.txt");
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));

            in.mark(1);
            if (in.read() != 0xFEFF)
                in.reset();

            Interpreter interpreter = new Interpreter();
            interpreter.setCustomInvalidLineHandler(ex -> {
                System.out.println();
                System.out.println(ex.getMessage());
            });
            interpreter.interpret(in);

        } catch (FileNotFoundException e) {
            System.out.println("Invalid file location!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
