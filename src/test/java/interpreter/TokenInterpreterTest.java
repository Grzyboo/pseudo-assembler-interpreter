package interpreter;

import interpreter.simulation.SystemSim;
import interpreter.tokenizer.Token;
import interpreter.tokenizer.TokenValue;
import interpreter.tokenizer.Tokenizer;
import interpreter.tokenizer.tester.TokenInterpreterTester;
import org.junit.jupiter.api.Test;

import java.util.Queue;

import static org.junit.Assert.*;

public class TokenInterpreterTest {

    @Test
    public void testRPNParsing_resultsInProperlyParsedExpression() {
        TokenInterpreterTester parser = new TokenInterpreterTester();

        Tokenizer tokenizer = new Tokenizer("((15/(7-(1+1)))*%edx)-(2+(1+%eax))");
        Queue<TokenValue> tokens = tokenizer.parseToTokenQueue();
        Queue<TokenValue> rpnNotation = parser.getRPN(tokens);

        assertTokenTypeAndValue(rpnNotation.poll(), Token.NUMBER, "15");
        assertTokenTypeAndValue(rpnNotation.poll(), Token.NUMBER, "7");
        assertTokenTypeAndValue(rpnNotation.poll(), Token.NUMBER, "1");
        assertTokenTypeAndValue(rpnNotation.poll(), Token.NUMBER, "1");
        assertTokenTypeAndValue(rpnNotation.poll(), Token.ADD_OPERATOR, "+");
        assertTokenTypeAndValue(rpnNotation.poll(), Token.SUBTRACT_OPERATOR, "-");
        assertTokenTypeAndValue(rpnNotation.poll(), Token.DIVIDE_OPERATOR, "/");
        assertTokenTypeAndValue(rpnNotation.poll(), Token.REGISTER, "%edx");
        assertTokenTypeAndValue(rpnNotation.poll(), Token.MULTIPLY_OPERATOR, "*");
        assertTokenTypeAndValue(rpnNotation.poll(), Token.NUMBER, "2");
        assertTokenTypeAndValue(rpnNotation.poll(), Token.NUMBER, "1");
        assertTokenTypeAndValue(rpnNotation.poll(), Token.REGISTER, "%eax");
        assertTokenTypeAndValue(rpnNotation.poll(), Token.ADD_OPERATOR, "+");
        assertTokenTypeAndValue(rpnNotation.poll(), Token.ADD_OPERATOR, "+");
        assertTokenTypeAndValue(rpnNotation.poll(), Token.SUBTRACT_OPERATOR, "-");
        assertNull(rpnNotation.poll());

    }

    private void assertTokenTypeAndValue(TokenValue tokenValue, Token type, String value) {
        assertNotNull(tokenValue);
        assertEquals(type, tokenValue.getType());
        assertEquals(value, tokenValue.getValue());
    }

    @Test
    public void testExpressionComputing() {
        SystemSim.registry.setRegisterValue("%eax", 1L);
        SystemSim.registry.setRegisterValue("%edx", 3L);

        Tokenizer tokenizer = new Tokenizer("((15/(7-(1+%eax)))*3)-(2+(1+%edx))");

        Queue<TokenValue> tokens = tokenizer.parseToTokenQueue();
        TokenInterpreterTester parser = new TokenInterpreterTester();

        assertEquals(3L, parser.compute(tokens));
    }
}
