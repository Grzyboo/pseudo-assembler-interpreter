package interpreter;

import interpreter.tokenizer.Token;
import interpreter.tokenizer.TokenValue;
import interpreter.tokenizer.Tokenizer;
import org.junit.jupiter.api.Test;

import java.util.Queue;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TokenTest {

    @Test
    public void testTokensMatch() {
        assertTrue(Token.NUMBER.pattern().matcher("1234").matches());
        assertFalse(Token.NUMBER.pattern().matcher("1234a").matches());

        assertTrue(Token.HEX_NUMBER.pattern().matcher("0x4234").matches());
        assertFalse(Token.HEX_NUMBER.pattern().matcher("4234").matches());

        assertTrue(Token.REGISTER.pattern().matcher("%edx").matches());
        assertFalse(Token.REGISTER.pattern().matcher("%eex").matches());

        assertTrue(Token.ADD_OPERATOR.pattern().matcher("+").matches());
        assertFalse(Token.ADD_OPERATOR.pattern().matcher("-").matches());

        assertTrue(Token.SUBTRACT_OPERATOR.pattern().matcher("-").matches());
        assertFalse(Token.SUBTRACT_OPERATOR.pattern().matcher("+").matches());

        assertTrue(Token.MULTIPLY_OPERATOR.pattern().matcher("*").matches());
        assertFalse(Token.MULTIPLY_OPERATOR.pattern().matcher("/").matches());

        assertTrue(Token.DIVIDE_OPERATOR.pattern().matcher("/").matches());
        assertFalse(Token.DIVIDE_OPERATOR.pattern().matcher("*").matches());

        assertTrue(Token.PARENTHESIS_LEFT.pattern().matcher("(").matches());
        assertFalse(Token.PARENTHESIS_LEFT.pattern().matcher(")").matches());

        assertTrue(Token.PARENTHESIS_RIGHT.pattern().matcher(")").matches());
        assertFalse(Token.PARENTHESIS_RIGHT.pattern().matcher("(").matches());
    }

    @Test
    public void testTokenExtraction_correctInput() {
        Tokenizer tokenizer = new Tokenizer("123+4+%edx-4*123/123-%eax");
        assertEquals(Token.NUMBER, tokenizer.nextToken().getType());
        assertEquals(Token.ADD_OPERATOR, tokenizer.nextToken().getType());
        assertEquals(Token.NUMBER, tokenizer.nextToken().getType());
        assertEquals(Token.ADD_OPERATOR, tokenizer.nextToken().getType());
        assertEquals(Token.REGISTER, tokenizer.nextToken().getType());
        assertEquals(Token.SUBTRACT_OPERATOR, tokenizer.nextToken().getType());
        assertEquals(Token.NUMBER, tokenizer.nextToken().getType());
        assertEquals(Token.MULTIPLY_OPERATOR, tokenizer.nextToken().getType());
        assertEquals(Token.NUMBER, tokenizer.nextToken().getType());
        assertEquals(Token.DIVIDE_OPERATOR, tokenizer.nextToken().getType());
        assertEquals(Token.NUMBER, tokenizer.nextToken().getType());
        assertEquals(Token.SUBTRACT_OPERATOR, tokenizer.nextToken().getType());
        assertEquals(Token.REGISTER, tokenizer.nextToken().getType());
        assertEquals(Token.END_OF_COMMAND, tokenizer.nextToken().getType());
        assertThrows(Tokenizer.FullyParsedException.class, tokenizer::nextToken);
    }

    @Test
    public void testTokenExtraction_incorrectInput() {
        Tokenizer tokenizer = new Tokenizer("%eax+12-asdf*5");
        assertEquals(Token.REGISTER, tokenizer.nextToken().getType());
        assertEquals(Token.ADD_OPERATOR, tokenizer.nextToken().getType());
        assertEquals(Token.NUMBER, tokenizer.nextToken().getType());
        assertEquals(Token.SUBTRACT_OPERATOR, tokenizer.nextToken().getType());
        assertThrows(Tokenizer.InvalidTokenException.class, tokenizer::nextToken);
    }

    @Test
    public void testTokenExtractionToQueue_correctInput() {
        Tokenizer tokenizer = new Tokenizer("4+%edx-0x14*123/1");
        Queue<TokenValue> tokens = tokenizer.parseToTokenQueue();
        assertEquals(Token.NUMBER, tokens.poll().getType());
        assertEquals(Token.ADD_OPERATOR, tokens.poll().getType());
        assertEquals(Token.REGISTER, tokens.poll().getType());
        assertEquals(Token.SUBTRACT_OPERATOR, tokens.poll().getType());
        assertEquals(Token.HEX_NUMBER, tokens.poll().getType());
        assertEquals(Token.MULTIPLY_OPERATOR, tokens.poll().getType());
        assertEquals(Token.NUMBER, tokens.poll().getType());
        assertEquals(Token.DIVIDE_OPERATOR, tokens.poll().getType());
        assertEquals(Token.NUMBER, tokens.poll().getType());
        assertEquals(Token.END_OF_COMMAND, tokens.poll().getType());
        assertNull(tokens.poll());
    }

    @Test
    public void testTokenExtractionToQueue_givenIncorrectInput() {
        Tokenizer tokenizer = new Tokenizer("asdf+0x012");
        assertThrows(Tokenizer.InvalidTokenException.class, tokenizer::parseToTokenQueue);
    }

}
